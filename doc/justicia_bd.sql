-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 02-09-2021 a las 17:26:27
-- Versión del servidor: 5.7.31
-- Versión de PHP: 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `justicia`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle`
--

DROP TABLE IF EXISTS `detalle`;
CREATE TABLE IF NOT EXISTS `detalle` (
  `ip` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `session` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `id` int(11) NOT NULL,
  `concepto` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `inicio` date NOT NULL,
  `capital` double NOT NULL,
  `pago` double NOT NULL,
  `tasa_tipo` int(11) NOT NULL,
  `tea` double NOT NULL,
  `tasa` double NOT NULL,
  `interes` double NOT NULL,
  `acumulado` double NOT NULL,
  `saldo_liquidar` double NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `detalle`
--

INSERT INTO `detalle` (`ip`, `session`, `id`, `concepto`, `inicio`, `capital`, `pago`, `tasa_tipo`, `tea`, `tasa`, `interes`, `acumulado`, `saldo_liquidar`) VALUES
('127.0.0.1', 'fdNV8wDk10aTo', 1, 'primer ingreso', '2021-06-09', 9900, 10, 1, 41.9, 0.85, 933.32, 933.32, 9900);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `formulas`
--

DROP TABLE IF EXISTS `formulas`;
CREATE TABLE IF NOT EXISTS `formulas` (
  `tasa` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `formula` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `otros` text COLLATE utf8_spanish2_ci NOT NULL,
  PRIMARY KEY (`tasa`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `formulas`
--

INSERT INTO `formulas` (`tasa`, `formula`, `otros`) VALUES
('Tasa Pasiva', 'Capital * ((100 + TasaHasta) / (100 + TasaDesde) -1)', ''),
('Tasa Activa', '(Capital * (Tasa/360) * Dias)/100', ''),
('Interes Simple', '(Capital * Tasa * Dias)/100', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `general`
--

DROP TABLE IF EXISTS `general`;
CREATE TABLE IF NOT EXISTS `general` (
  `ip` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `session` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `fecha_close` date NOT NULL,
  `total_int` double NOT NULL,
  `total_pago` double NOT NULL,
  `efectivo` double NOT NULL,
  `neto` double NOT NULL,
  `date_creation` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `general`
--

INSERT INTO `general` (`ip`, `session`, `fecha_close`, `total_int`, `total_pago`, `efectivo`, `neto`, `date_creation`) VALUES
('127.0.0.1', 'fdNV8wDk10aTo', '2021-08-29', 869.43, 0, 9000, 9869.43, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tasas`
--

DROP TABLE IF EXISTS `tasas`;
CREATE TABLE IF NOT EXISTS `tasas` (
  `tasa` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `desde` date NOT NULL,
  `hasta` date NOT NULL,
  `tipo` int(11) NOT NULL,
  `tea` double NOT NULL,
  `tna` double NOT NULL,
  `desde_dias` int(11) NOT NULL,
  `hasta_dias` int(11) NOT NULL,
  PRIMARY KEY (`tasa`,`desde`,`hasta`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `tasas`
--

INSERT INTO `tasas` (`tasa`, `desde`, `hasta`, `tipo`, `tea`, `tna`, `desde_dias`, `hasta_dias`) VALUES
('COMERCIAL 0 30 ', '2020-03-02', '2050-03-02', 0, 41.9, 35.5, 0, 30),
('COMERCIAL 31 60', '2020-03-02', '2050-03-02', 0, 41.9, 35.5, 31, 60),
('COMERCIAL 61 90', '2020-03-02', '2050-03-02', 0, 41.9, 35.5, 61, 90),
('COMERCIAL 91 120\r\n', '2020-03-02', '2050-03-02', 0, 41.9, 35.5, 91, 120);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
