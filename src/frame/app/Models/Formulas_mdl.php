<?php

namespace App\Models;

use CodeIgniter\Model;

class Formulas_mdl extends Model
{
    protected $table      = 'formulas';
    protected $primaryKey = 'tasa';
    protected $allowedFields = ['formula', 'otros'];
}