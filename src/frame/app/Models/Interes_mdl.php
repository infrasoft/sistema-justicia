<?php

namespace App\Models;

use CodeIgniter\Model;


class Interes_mdl extends Model
{
    protected $table      = 'tasas';
    protected $primaryKey = ['tasa','from', 'until'];
    protected $allowedFields = ['from', 'until', 'tem', 'dias'];

    protected $returnType     = 'array';
    protected $useSoftDeletes = true;    

    protected $useTimestamps = false;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}