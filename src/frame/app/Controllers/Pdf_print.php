<?php

namespace App\Controllers;
use CodeIgniter\I18n\Time;



class Pdf_print extends BaseController
{   
	//imprimir PDF
	public function print_form($op="",$ip = "")
    {  
                     
        $dompdf = new \Dompdf\Dompdf();
        $options = $dompdf->getOptions(); 
        $options->set(array('isRemoteEnabled' => true));
        $dompdf->setOptions($options);
       
        $tabla = new Sql_lib();
		$tabla->tabla = "general";
        $tabla->condicion = "ip='".$ip."'";
        $data["tabla"] = $tabla->consultaSQLbasicaRow(); //print_r($data["tabla"]);

		$detalle = new Sql_lib();
		$detalle->tabla = "detalle";
        $detalle->condicion = "ip='".$ip."'";
        $data["vector"] = $detalle->consulSQLbasica(); //print_r($data["vector"]);

        $header = '<!DOCTYPE html>
        <html lang="es">
        <head>
            <!-- Define Charset -->
            <meta charset="utf-8">             
        
        </head>
        <body>
            <img src="'.site_url("media/img/logo.png").'" width="900" height="200"/>            
        ';
        $tabla_head = '<h2>Total Adeudado</h2>
        <table style="width:100%;">
            <tr style="width:100%; color: blue;">
                <td>
                    Fecha de liquidacion
                </td>
                
                <td>
                    Total Adeudado
                </td>		
            </tr>
            <tr>
                <td>
                     '.$data["tabla"]["fecha_close"].'   
                </td>
               
                <td>
                    '.$data["tabla"]["neto"].'
                </td>
            </tr>
        </table>
        <hr/>
        <h2>Informacion Adicional</h2>
        <table style="width:100%">
            <tr >
                <td>
                    Total Capital
                </td>
                <td>
                     Total Intereses
                </td>
                <td>
                    Total Pagos
                </td>		
            </tr>
            <tr>
                <td>
                '.$data["tabla"]["efectivo"].'
                </td>
                <td>
                '.$data["tabla"]["total_int"].'
                </td>
                <td>
                '.$data["tabla"]["total_pago"].'
                </td>
            </tr>
        </table>
        <hr/>

        <h2>Tabla</h2>
        
		<table  style="width:100%;">
        <tr style="width:100%; color: blue;">
          <td>Concepto</td>          
          <td>Fecha Inicio</td>
          <td>Fecha Final</td>          
          <td>Capital</td>
          <td>Pagos</td>
          <td>TEA</td>
          <td>Tasa</td>
          <td>Interes</td>
          <td>Acumulado</td>
        </tr>
        ';
        $tabla = '';
        $tabla_footer = '
        </table>';
        $footer = '</body>    
                </html>';
        foreach($data["vector"] as $row)
        {
            $tabla .= "<tr>
                       <td> ".$row["concepto"]."</td>".
                      "<td> ".$row["inicio"]."</td>".
                      "<td> ".$row["fin"]."</td>".
                      "<td> ".$row["capital"]."</td>".
                      "<td> ".$row["pago"]."</td>".
                      "<td> ".$row["tea"]."</td>".
                      "<td> ".$row["tasa"]."</td>".
                      "<td> ".$row["interes"]."</td>".
                      "<td> ".$row["acumulado"]."</td>
                      </tr>";
        }

        
       /* echo ($header
        .$tabla_head.
        $tabla.
        $tabla_footer
    .$footer);*/
       /**/ 
        if ($op != "preview") 
        {
            $dompdf->loadHtml(utf8_decode($header
                                .$tabla_head.
                                $tabla.
                                $tabla_footer
                            .$footer));        
            $dompdf->setPaper('A4', 'landscape');
            $dompdf->render();
            $dompdf->stream("mi_calculo");        
        }
        else        
        {
            
            echo utf8_decode(
                $header
            .$tabla_head.
            $tabla.
            $tabla_footer.'<script>
                function load() {
                    window.print();
                    }
                    window.onload = load;
              </script>'
            .$footer);
            $dompdf->loadHtml(utf8_decode($header
                                .$tabla_head.
                                $tabla.
                                $tabla_footer
                            .$footer));
                            $dompdf->setPaper('A4', 'landscape');
                            $dompdf->render();
            $dompdf->stream("mi_calculo", ["Attachment" => 0]); 
        }        
    }    
}
?>