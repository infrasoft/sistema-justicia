<?php
/**********************************************
 *****Libreria para acceso a base de datos*****
 **********************************************
 *Autor: Ariel Marcelo Diaz

 *Sitio Web: https://www.infrasoft.com.ar
 ****************************************/

//clase para acceder a la base de datos
namespace App\Controllers; 

use CodeIgniter\Controller;

class Sql_lib extends BaseController{
	//instancias
	public $tabla = "";
	public $campos = "*";
	public $valores = "";
	public $condicion = "";
	public $opcion = 0;
	public $estilo = "";
	public $id = "";
	public $muestra = "";
	public $predeterminado = "";
    public $vector = array();

	// conecta a la base de datos
	function conectarbase() {
		$dbhost = "localhost";
		$dbusername = "root";
		$dbpass = "";
		$dbname = "justicia";
		// conectar con el servidor
		$conexion = mysqli_connect($dbhost, $dbusername, $dbpass) or die("No se puede conectar al servidor");
		// Seleccionar la bd
		mysqli_select_db($conexion,$dbname) or die("No se puede seleccionar la base de datos");
		mysqli_set_charset($conexion,'utf8'); 
		return $conexion;
	}

	//Realiza una consulta basica
	//$conexion, $tabla, $campos, $condicion
	function consulSQLbasica() 
	{
		if ($this -> condicion == "") 
		{
			$query = "SELECT " . $this -> campos . " FROM " . $this -> tabla . ";";
		}
		 else
	    {
			$query = "SELECT " . $this -> campos . " FROM " . $this -> tabla . " WHERE " . $this -> condicion . ";";
		}	
		//echo  $query."<br/>"; 
		$consulta = mysqli_query($this -> conectarbase() , $query ) 
					or die("Fallo en la consulta");
		return $consulta;
	}

	// Realiza una consulta basica y devuelve el array
	function consultaSQLbasicaRow() 
	{
		$consulta = $this -> consulSQLbasica();
		$row = mysqli_fetch_array($consulta); 
		return $row;
	}

	// Realiza la insercion de datos
	//$conexion,$tabla, $campos, $valores
	function insertaSQL() 
	{
		$query = "INSERT INTO " . $this -> tabla . " (" . $this -> campos .
			 ") VALUES (" . $this -> valores . ");";
		//echo $query."<br/>"; 
		$consulta = mysqli_query( $this -> conectarbase(), $query);
		return $consulta;
	}

	// Realiza la modificacion de datos
	//$conexion, $tabla, $campos, $valores, $condicion
	function modificarSQL() //armar a traves de array
	{
		$query = "UPDATE " . $this -> tabla . " SET ";
		$campos = utf8_decode($this -> campos);
		$atributos = explode(",", $campos);
		$val = explode(",", $this -> valores);
		$j = 0;
		foreach ($atributos as $i) 
		{
			//$val[$j] = str_replace(".",",",$val[$j]); // para los puntos flotantes
			$query .= "$i = $val[$j],";
			$j++;
		}
		$query = substr($query, 0, strlen($query) - 1);
		if ($this -> condicion != "") {
			$query .= " WHERE ";
			$query .= $this -> condicion;
		}
		$query .= ";";
		//echo $query."<br/>";
		$consulta = mysqli_query( $this -> conectarbase(), $query) or die("Fallo en la modificacion");
		return $consulta;
	}
    
    //realiza el cambio con otra metodologia
    function modificaSQL2() //no terminada
    {
        $query = "UPDATE " . $this -> tabla . " SET ";
        /*foreach($vector as $i=>$j)
        {
        
        }*/
    }

	//Realiza el borrado de datos de una base de datos
	//$conexion, $tabla, $condicion
	function BorraSQL() 
	{
		$query = "DELETE FROM " . $this -> tabla . " WHERE " . $this -> condicion . ";";
		$consulta = mysqli_query( $this -> conectarbase(), $query) or die("Fallo la consulta");
		//echo $query;
		return $consulta;
	}

	// Muestra un select de una tabla predeterminada, devolviendo el valor
	//$conexion, $tabla, $id, $muestra, $condicion, $predeterminado, $estilo
	function selectTabla()
	 {
		$devuelve = "<select name='" . $this -> tabla . "' class='" . $this -> estilo . "'>\n";
		$consulta = $this ->consulSQLbasica();
		$aux = explode(",", $this -> muestra);

		$row = mysqli_fetch_array($consulta);
		while ($row != null) {
			if ($row["$this->id"] == $this -> predeterminado) 
			{
				$devuelve .= utf8_encode("<option value='" . $row[$this -> id] . "' selected='selected'>");
				foreach ($aux as $i) 
				{
					$devuelve .= utf8_encode($row[$this -> i]);
				}
				$devuelve .= "</option>\n";
			} 
			else 
			{
				$devuelve .= utf8_encode("<option value='" . $row[$this -> id] . "'>");
				foreach ($aux as $i)
				{
					$devuelve .= utf8_encode($row[$this -> i]);
				}
				$devuelve .= "</option>\n";
			}
			$row = mysqli_fetch_array($consulta);
		}
		$devuelve .= "</select>\n";
		if ($this -> opcion == 0) 
		{
			return $devuelve;
		} 
		else 
		{		
			echo $devuelve;
		}

	}

}
?>