<?php

namespace App\Controllers;
use CodeIgniter\I18n\Time;

helper('form');
helper('vistas');


class Home extends BaseController
{
	private function tasa($data = null,$formula="", $capital=0,
								 $dias=0, $interes)
	{	
				$tasa_nombre = $data["tasa"]; 
				$tna = $data["tna"];
				//echo "tna: ".$tna;
				$tea = $data["tea"];	
				//echo "tea: ".$tea;				
				$tna_cal = (((1 + $tea)**(1/360))- 1)*360;
				//echo "tna: ". $tna_cal."<br/>";
				$tnd = ($tna_cal/360)*$dias;
				//echo "TND: ".$tnd."<br/>";	
				$int_apl = ((1 + $tnd)**$dias - 1)*$dias;
				//echo "Tasa efectiva: ".$int_apl."<br/>";
				$tasa_act = $tnd*$dias*100;
				//echo "Tasa activa: ".$tasa_act."<br/>";	
				//echo "Formula:".$formula."<br/>";		  
			 	//echo ("capital:".$capital." Dias:".$dias." Tasa:".$tna." Intereses:".$interes."<br/>");                 
			 	$formula = str_replace("Capital",$capital,$formula); 
			 	$formula = str_replace("Dias",$dias,$formula);			 	
			 	$formula = str_replace("Interes",$interes,$formula);

				$formula = str_replace("TasaHasta", $tnd, $formula); //verificar
			 	$formula = str_replace("TasaDesde", $tnd, $formula);

				$formula = str_replace("Tasa",$tea,$formula); 
				
				//echo "Formula: ".$formula."<br/>"; 
				$var = eval("return ".$formula.";");
				//print "formula: ".$var."<br/>";
				$calculo = ["calculo"=>$var, "tna_cal"=>$tna_cal, "tnd"=>$tnd,
							"tasa_act"=>$tasa_act,"tea"=>$tea,"cal"=>$var,
							"desde"=>$data["desde"],"hasta"=>$data["hasta"]];				
			
			if (!isset($var)) 
			{
				return [];
			}
			else
			{
				return $calculo;
			}
	}
	//actualiza los datos luego de una modificacion o eliminacion
	private function update_acumulados($ip="",$session="")
	{
		$data = ["mensaje" => ""];
		$tabla = new Sql_lib();
		$tabla->tabla = "general";
		$tabla->condicion = "ip ='".$ip."' AND session ='".$session."'";

		$detalle = new Sql_lib();
		$detalle->tabla = "detalle";
		$detalle->condicion = "ip ='".$ip."' AND session='".$session.
								"' ORDER BY id DESC";

		//i=1 ..n
		$acum = 0; $saldo = 0; $pago = 0; $i = 0;
		$detalle->campos = "id,interes,capital,pago";
		$vector = $detalle->consulSQLbasica();
		$detalle->campos = "acumulado,saldo_liquidar";

		foreach ($vector as $row) 
		{
			$acum += $row["interes"];
			$saldo += $row["capital"] - $row["pago"];
			$pago += $row["pago"];
			$detalle->valores = $acum.",".$saldo;
			$detalle->condicion = "ip ='".$ip."' AND session='".$session.
								"' AND id=".$row["id"]; 
			$detalle->modificarSQL();			
		}		

		$tabla->campos = "total_int,total_pago,efectivo";
		$tabla->valores = $acum.",".$pago.",".$saldo;
		if ($tabla->modificarSQL()) 
		{
			$data["mensaje"] .= mensajes("Lista modificada correctamente");	
		} else 
		{
			$data["mensaje"] .= mensajes("Lista No modificada correctamente");
		}
		
	}

	public function index()
	{
		$data = ["mensaje" => ""];
		$tabla = new Sql_lib();
		$tabla->tabla = "general";

		$detalle = new Sql_lib();
		$detalle->tabla = "detalle";	

		echo view('header');

		$tasa_sel = $this->request->getPost('tasa_sel');
		$date_end = $this->request->getPost('date_end');
		$date_ini = $this->request->getPost('date_ini');
		$capital = $this->request->getPost('capital'); 
		$tasa = $this->request->getPost('tasa');
		$concepto = $this->request->getPost('concepto');
		$pago = $this->request->getPost('pago');
		$date_creation = $this->request->getPost('date_creation');   
		$interes = 3;//$this->request->getPost('interes'); 
		if (($date_end!="") AND ($date_ini != "")) 
		{				
			//calculo cantidad de dias
			$time = Time::parse($date_ini);
			$diff = $time->difference($date_end);
			$dias = $diff->getDays();
			//echo "dias: ".$dias;
			if ($date_ini >= $date_end) 
			{
				$data["mensaje"] .= mensajes("La fecha inicial y final deben ser diferentes a 0");
			}else
			{				
						
			//formula
			$formula = new Sql_lib();
			$formula->tabla = "formulas";
			//tasa
			$tasa = new Sql_lib();
			$tasa->tabla = "tasas";
			switch ($tasa_sel) 
			{
				case '1': //Activa descuento de documentos en pesos H/59 días
					$tasa->condicion = 
					"((hasta >= '".$date_ini."' ) AND ".
					"(desde <= '".$date_end.					
					"' )) AND  (hasta_dias=60) AND (tipo=0)";
					$formula->condicion = "tasa='Tasa Activa'";		
				break;
				case "2"://Activa descuento de documentos en pesos D/120 días (BTF)
					$tasa->condicion = 
					"((hasta >= '".$date_ini."' ) AND ".
					"(desde <= '".$date_end.
					"' )) AND  (hasta_dias=120) AND (tipo=0)";
					$formula->condicion = "tasa='Tasa Activa'";
				break;
				case "3"://Pasiva Plazo fijo en pesos a 30 días (BTF)
					$tasa->condicion = 
					"((hasta >= '".$date_ini."' ) AND ".
					"(desde <= '".$date_end.
					"' )) AND  (hasta_dias=30) AND (tipo=1)";
					$formula->condicion = "tasa='Tasa Pasiva'";
				break;
				case "4"://Promedio Activa -59/Pasiva (BTF)
					$tasa->condicion = 
					"((hasta >= '".$date_ini."' ) AND ".
					"(desde <= '".$date_end.
					"' )) AND  (hasta_dias=60) AND (tipo=1)";
					$formula->condicion = "tasa='Tasa Pasiva'";
				break;
				case "5"://Promedio Activa +180/Pasiva (BTF)
					$tasa->condicion = 
					"((hasta >= '".$date_ini."' ) AND ".
					"(desde <= '".$date_end.
					"' )) AND  (hasta_dias=120) AND (tipo=0)";
					$formula->condicion = "tasa='Interes Simple'";
				break;						
			}			
									
			$tabla->campos = 
				"ip,session,fecha_close,total_int,total_pago,efectivo,neto,date_creation";
			$tabla->condicion = "ip='".$_SESSION["ip"]."'";
			$tabla_array = $tabla->consulSQLbasica();
			//creando la tabla
			if($tabla_array->num_rows == 0)
			{									
				$session = crypt(time(),"fdsfsrJTYUFYU345364&$&#$#bsdgsfg"); 
				$session = str_replace("/","%",$session);
				$tabla->valores = "'".$_SESSION["ip"]."','".$session."','".
								$date_end."',0,0,".$capital.",0,'".$date_creation."'";				
											
				if ($tabla->insertaSQL()) 
				{	
					$tabla_array = $tabla->consulSQLbasica();				
					//$data["mensaje"] .= mensajes("Registro creado correctamente");
				}
				else 
				{
					$data["mensaje"] .= mensajes("Registro No creado correctamente");
				}								
			}
			//datos de la tabla
			$data["tabla"] = $tabla_array->fetch_row(); 
			$form = $formula->consultaSQLbasicaRow();
			$data["tasa"] = $tasa->consulSQLbasica(); //print_r($data["tasa"]);
			//ver si existe 2 o mas tasas	
			switch ($data["tasa"]->num_rows) 
			{
				case 0:
					$data["mensaje"] .= mensajes("No existen tasas en ese periodo");
				break;
				case 1: //existe un resultado - verificar si no cubre toda la tasa
					$data["tasa"] = $row = mysqli_fetch_array($data["tasa"]);  
					$interes = $this->tasa($data["tasa"], $form["formula"],
									$capital, $dias, $interes);
					if ($tabla_array->num_rows>=0) 
					{				
						$detalle->campos = "ip,session,id,concepto,inicio,fin,capital,pago,tasa_tipo,".
											"tea,tasa,interes,acumulado,saldo_liquidar";
						$detalle->condicion = "ip='".$data["tabla"][0].
												"' AND session='".$data["tabla"][1]."'";										
						$id = $detalle->consulSQLbasica();
						//agregando el acumulado y el saldo
						$detalle->condicion = "ip='".$data["tabla"][0]."' AND session='".
												$data["tabla"][1]."' AND id=".$id->num_rows;
						$complemento = $detalle->consultaSQLbasicaRow();
						$acumulado = $complemento["acumulado"] + $interes["cal"];
						$saldo = $complemento["saldo_liquidar"] + $capital - $pago;
						
						if ($data["tabla"][0] != "") 
						{
							if ($date_ini > $interes["desde"]) 
							{
								$inicio = $date_ini;
							}
							else
							{
								$inicio = $interes["desde"];
							}

							if ($date_end < $interes["hasta"]) 
							{
								$fin = $date_end;
							} else 
							{
								$fin = $interes["hasta"];
							}
							
							$detalle->valores = "'".$data["tabla"][0]."','".$data["tabla"][1]."',".
												($id->num_rows+1).",'".$concepto."','".$inicio.
												"','".$fin."',".round($capital,2).",".round($pago,2).",".									
												$tasa_sel.",".round($interes["tea"],2).",".
												round($interes["tnd"],2).",".round($interes["cal"],2).
												",".round($acumulado,2).",".round($saldo,2);
											
							if ($detalle->insertaSQL()) 
							{
								//$data["mensaje"] .= mensajes("Datos insertados correctamente");
								$tabla->campos = "efectivo,total_pago,total_int,efectivo,neto";
								$efectivo = $complemento["efectivo"] + $capital;
								$pago = $complemento["total_pago"] + $pago;
								$tabla->valores = round($efectivo,2).",".round($pago,2).",".
													round($acumulado,2).",".round($saldo,2).",".
													round($acumulado + $saldo,2);
								$tabla->condicion = "ip='".$data["tabla"][0]."' AND session ='". 
													$data["tabla"][1]."'";
								$tabla->modificarSQL(); 
							}
							else
							{
								//$data["mensaje"] .= mensajes("Datos No insertados correctamente");
							}
						}				
					}
				break;
				default: //existen varios resultados
						//$data["mensaje"] .= mensajes("Existen varias tasas en el periodo seleccionado");
						$tabla->campos = "efectivo,total_pago";
						//print_r($data["tabla"]);
				//		$capital = $data["tabla"][5] + $capital; verificar
						$pago = $data["tabla"]["total_pago"] + $pago;
						$tabla->valores = $capital.",".$pago;
						$tabla->condicion = "ip='".$data["tabla"][0]."' AND session ='". 
													$data["tabla"][1]."'";
						$tabla->modificarSQL();
						$n = $data["tasa"]->num_rows;						
						for ($i=1; $i <= $n+1; $i++) 
						{										
							$row = mysqli_fetch_array($data["tasa"]);							
							if (strtotime($date_ini) > strtotime($row["desde"])) 
							{
								$inicio = $date_ini;
							}
							else
							{
								$inicio = $row["desde"];
							}
							
							if (strtotime($date_end) < strtotime($row["hasta"])) 
							{
								$fin = $date_end; 
							} else 
							{
								$fin = $row["hasta"];
							}
							//calculo cantidad de dias
							$time = Time::parse($inicio);
							$diff = $time->difference($fin);
							$dias = $diff->getDays();
							
							$interes = $this->tasa($row, $form["formula"],
									$capital, $dias, $interes);

							$detalle->campos = "ip,session,id,concepto,inicio,fin,capital,pago,tasa_tipo,".
									"tea,tasa,interes,acumulado,saldo_liquidar";
							$detalle->condicion = "ip='".$data["tabla"][0].
										"' AND session='".$data["tabla"][1]."'";										
							$id = $detalle->consulSQLbasica();
							//agregando el acumulado y el saldo
							$detalle->condicion = "ip='".$data["tabla"][0]."' AND session='".
							$data["tabla"][1]."' AND id=".$id->num_rows;
							$complemento = $detalle->consultaSQLbasicaRow();
							if ($i==1) 
							{
								$saldo = $complemento["saldo_liquidar"] + $capital;
							}
						/*	else
							{
								$saldo += 0;
							}*/
							$acumulado = $complemento["acumulado"] + $interes["cal"];
							if ($data["tabla"][0] != "") 
							{									
								
								$detalle->valores = "'".$data["tabla"][0]."','".$data["tabla"][1]."',".
												($id->num_rows+1).",'".$concepto."','".$inicio."','".
												$fin."',".round($capital,2).",".round($pago,2).",".									
												$tasa_sel.",".round($interes["tea"],2).",".
												round($interes["tnd"],2).",".round($interes["cal"],2).
												",".round($acumulado,2).",".round($saldo,2);
											
								if ($detalle->insertaSQL()) 
								{
									//$data["mensaje"] .= mensajes("Datos insertados correctamente");
									$tabla->campos = "total_int,neto";
									$tabla->valores = round($acumulado,2).",".
															round($acumulado + $saldo,2);
									$tabla->condicion = "ip='".$data["tabla"][0]."' AND session ='". 
															$data["tabla"][1]."'";
									$tabla->modificarSQL(); 
								}
								else
								{
									//$data["mensaje"] .= mensajes("Datos No insertados correctamente");
								}
							}							
							$i++;
						}
				break;
				}
			}						
						
		}
		$tabla->campos = 
				"ip,session,fecha_close,total_int,total_pago,efectivo,neto,date_creation";
		$tabla->condicion = "ip='".$_SESSION["ip"]."'";
		$data["tabla"] = $tabla->consultaSQLbasicaRow();
		$detalle->condicion = "ip='".$_SESSION["ip"]."'";
		$data["vector"] = $detalle->consulSQLbasica();	
			
		echo view('form', $data);
		echo view('footer');
	}

	// elimina o modifica un elemento del array
	function op($ip = "",$sess = "",$id="", $op="del")
	{	
		$tabla = new Sql_lib();
		$tabla->tabla = "general";
		$tabla->campos = "ip,session,fecha_close,total_int,efectivo,neto";

		$detalle = new Sql_lib();
		$detalle->tabla = "detalle";
		$detalle->campos = "ip,session,id,concepto,inicio,capital,pago,tasa_tipo,".
								"tea,tasa,interes,acumulado,saldo_liquidar";
		$data = [];							
		echo view('header');		
		switch ($op) 
		{			
			case "del":
				$detalle->condicion = "ip='".$ip."' AND session='".
										$sess."' AND id=".$id;
				if($detalle->BorraSQL())
				{
					//$data["mensaje"] .= mensajes("Dato Eliminado Correctamente");
					$this->update_acumulados($ip,$sess);
				}
				else
				{
					//$data["mensaje"] .= mensajes("Fallo Al Eliminar los datos");
				}
				return redirect()->to('/public');
			break;
			case "del_all":
				$detalle->condicion = "ip='".$ip."'";
				if($detalle->BorraSQL())
				{
					$_SESSION["mensaje"] .= mensajes("Datos Eliminados Correctamente");
					$tabla->condicion = "ip='".$ip."'";
					if ($tabla->BorraSQL()) 
					{
						//$data["mensaje"] .= mensajes("Tabla Eliminada correctamente");
					}
					else
					{
						//$data["mensaje"] .= mensajes("Fallo en la eliminacion de tabla");
					}
				}
				else
				{
					//$data["mensaje"] .= mensajes("Fallo Al Eliminar los datos");
				}
				return redirect()->to('/public');
			break;
			case 'update': //verificar cambios en tabla - 
				//data input
				$concepto = $this->request->getPost('concepto');
				$tasa_sel = $this->request->getPost('tasa_sel');
				$date_ini = $this->request->getPost('date_ini');
				$capital = $this->request->getPost('capital');
				$pago = $this->request->getPost('pago');
				$interes = "3";

				$tabla->condicion = "ip='".$ip."' AND session='".
								    $sess."'";
				$result_tabla = $tabla->consultaSQLbasicaRow();
				//print_r($result_tabla);
				if (isset($capital)) 
				{
					//formula
					$formula = new Sql_lib();
					$formula->tabla = "formulas";
					
					$tasa = new Sql_lib();
					$tasa->tabla = "tasas";
					$tasa->condicion = "((desde >= '".$date_ini."' ) OR ".
									"(hasta <= '".$result_tabla[2].
									" )) AND  (hasta_dias=30)";									
					switch ($tasa_sel) 
					{
						case '1': //Activa descuento de documentos en pesos H/59 días
							$tasa->condicion = 
							"((hasta >= '".$date_ini."' ) AND ".
							"(desde <= '".$result_tabla[2].					
							"' )) AND  (hasta_dias=60) AND (tipo=0)";
							$formula->condicion = "tasa='Tasa Activa'";		
						break;
						case "2"://Activa descuento de documentos en pesos D/120 días (BTF)
							$tasa->condicion = 
							"((hasta >= '".$date_ini."' ) AND ".
							"(desde <= '".$result_tabla[2].
							"' )) AND  (hasta_dias=120) AND (tipo=0)";
							$formula->condicion = "tasa='Tasa Activa'";
						break;
						case "3"://Pasiva Plazo fijo en pesos a 30 días (BTF)
							$tasa->condicion = 
							"((hasta >= '".$date_ini."' ) AND ".
							"(desde <= '".$result_tabla[2].
							"' )) AND  (hasta_dias=30) AND (tipo=1)";
							$formula->condicion = "tasa='Tasa Pasiva'";
						break;
						case "4"://Promedio Activa -59/Pasiva (BTF)
							$tasa->condicion = 
							"((hasta >= '".$date_ini."' ) AND ".
							"(desde <= '".$result_tabla[2].
							"' )) AND  (hasta_dias=60) AND (tipo=1)";
							$formula->condicion = "tasa='Tasa Pasiva'";
						break;
						case "5"://Promedio Activa +180/Pasiva (BTF)
							$tasa->condicion = 
							"((hasta >= '".$date_ini."' ) AND ".
							"(desde <= '".$result_tabla[2].
							"' )) AND  (hasta_dias=120) AND (tipo=0)";
							$formula->condicion = "tasa='Interes Simple'";
						break;						
					}
					
					$detalle->condicion = "ip='".$ip."' AND session='".
									$sess."'";
					$lista = $detalle->consulSQLbasica(); //print_r($lista);

					//calculo cantidad de dias
					$time = Time::parse($date_ini);
					$diff = $time->difference($result_tabla[2]);
					$dias = $diff->getDays();
					//calculo de formula
					$form = $formula->consultaSQLbasicaRow();
					 
					$int = $this->tasa($tasa->consultaSQLbasicaRow(),
							 $form["formula"],	
							 $capital, 
							 $dias, 
							 $interes); //print_r($int);
					//acomodar los valores de interes acumulado y saldo
					$acumu = 0; 
					$saldo = 0;
					if ($lista->num_rows > 1) 
					{					
						foreach ($lista as $item) 
						{
							if (($id != $item["id"]) AND ($id > $item["id"])) 
							{
								$acumu += $item["acumulado"]; 
								$saldo += $item["saldo_liquidar"];								
							}
							elseif (($id != $item["id"]) AND ($id < $item["id"])) 
							{
								$acumu += $item["acumulado"]; 
								$saldo += $item["saldo_liquidar"];
								$detalle->campos = "acumulado,saldo_liquidar";
								$detalle->valores = $acumu.",".$saldo;
								$detalle->condicion = "ip='".$ip."' AND session='".
									$sess."' AND id=".$id;
								$detalle->modificarSQL();
							}
							else
							{
								$acumu += $int["cal"]; 
								$saldo += $capital;
							}
						}
					}
					else
					{
						$acumu += $int["cal"]; 
						$saldo += $capital;
					}

					if ($date_ini > $int["desde"]) 
					{
						$inicio = $date_ini;
					}
					else
					{
						$inicio = $int["desde"];
					}

					if ($result_tabla[2] < $int["hasta"]) 
					{
						$fin = $result_tabla[2];
					} else 
					{
						$fin = $int["hasta"];
					}

					$detalle->campos = "ip,session,id,concepto,inicio,fin,capital,pago,tasa_tipo,".
								"tea,tasa,interes,acumulado,saldo_liquidar";
					$detalle->valores = "'".$ip."','".$sess."',".$id.",'"
									.$concepto."','".$inicio."','".$fin."',".
									round($capital, 2).",".round($pago, 2).
									",".$tasa_sel.",".round($int["tea"], 2).
									",".round($int["tnd"], 2).",".
									round($int["cal"], 2).",".round($acumu, 2).
									",".round($saldo, 2);
					if ($detalle->modificarSQL()) 
					{
						//$data["mensaje"] .= mensajes("Datos modificados correctamente");
						$this->update_acumulados($ip,$sess);
					}
					else
					{
						//$data["mensaje"] .= mensajes("Datos no modificados");
					}
				}
			
				$detalle->condicion = "ip='".$ip."' AND session='".
										$sess."'";
				$data["vector"] = $detalle->consultaSQLbasicaRow();				
				  
				//print_r($tabla);
				echo view("modificar",$data);				
								
				redirect()->to('/home');
			break;
			default:
				redirect()->back();
			break;
		}
		echo view('footer');
	}

	//realiza la vista previa del elemento
	function preview($ip = "" , $view = "")
	{
		echo view('header');
		$tabla = new Sql_lib();
		$tabla->tabla = "general";
		$tabla->campos = "ip,session,fecha_close,total_int,total_pago,efectivo,neto";
		$tabla->condicion = "ip='".$ip."'";
		$data["tabla"] = $tabla->consultaSQLbasicaRow();

		$detalle = new Sql_lib();
		$detalle->tabla = "detalle";
		$detalle->campos = "ip,session,id,concepto,inicio,fin,capital,pago,tasa_tipo,".
								"tea,tasa,interes,acumulado,saldo_liquidar";
		$detalle->condicion = "ip='".$ip."'";
		$data["detalle"] = $detalle->consulSQLbasica();
		$data["otro_btn"] = '<a href="'.site_url("public").'">
							<button  type="submit" class="btn btn-secondary m-1 p-1">
								<i class="fas fa-exchange-alt"></i> Volver
	  						</button>
							  </a>';
		if ($view == "") 
		{
			echo view('botones', $data);
		}	  	
		echo view('tabla', $data);
		
		echo view('footer');
	}
	//completa las tea faltantes de la bd
	function update_tasas()
	{
		echo view('header');
		$tasas = new Sql_lib();
		$tasas->tabla = "tasas";
		$tasas->condicion = "tea=''";
		$vector = $tasas->consulSQLbasica();
		$tasas->campos = "tea,tna,tem";

		$archivo = "upload.csv";
    	$filas=file($archivo);
		$i = 1;
    	print "leyendo filas"."<br/>";
		/**********************
		 * 0 = concepto
		 * 1 = desde
		 * 2 = hasta
		 * 3 = tea
		 * 4 = tna
		 * 5 = desde_dias
		 * 6 = hasta_dias
		 * 7 = cargado
		 * 8 = tem
		 **************/
		while($filas[$i]!= null)
    	{
			$data["detail"] = "";
			$linea = explode(";",$filas[$i]);
			$tasas->condicion = "tasa='".$linea[0].
						 "' AND desde='".$linea[1].
						 "' AND hasta='".$linea[2]."'";
			$data["consulta"] = $tasas->consultaSQLbasicaRow();
			print_r($data["consulta"]);
			if ($data["consulta"] == array()) 
			{
				$data["detail"] .= "Datos no encontrados "
									.$linea[0]." ".$linea[1]." ".$linea[2]."<br/>";
				
				$tasas->campos = "tasa,desde,hasta,tipo,tea,tna,tem,desde_dias,hasta_dias";
				$tasas->valores = "'".$linea[0]."','".$linea[1]."','".$linea[2]."',0"
									.$linea[3].",".$linea[4].",".$linea[8].
									",".$linea[5].",".$linea[6];
				if ($tasas->insertaSQL()) 
				{
					$data["detail"] .= "Datos Insertados Correctamente <br/>";
				}
				else {
					$data["detail"] .= "Datos no insertados correctamente <br/>";
				}				
			}
			else
			{// comprobar y modificar las tasas
				
				if (($data["consulta"]["tea"] == null) OR
					($data["consulta"]["tea"] == 0)) 
				{
					$data["consulta"]["tea"] = (1 + $data["consulta"]["tna"]/12)**12 -1;									
				}
				else if(($data["consulta"]["tea"] != $linea[3])
						AND ($linea[3] != "")) 
				{
					$data["consulta"]["tea"] = $linea[3];
				}

				if (($data["consulta"]["tem"] == null) OR
					($data["consulta"]["tem"] == 0)) 
				{
					$data["consulta"]["tem"] = (1 + $data["consulta"]["tea"])**(30/360) - 1 ;
										
				}
				else if(($data["consulta"]["tem"] != $linea[8]) 
						AND ($linea[8] != "")) 
				{
					$data["consulta"]["tem"] = $linea[8]; 
				}

				$tasas->campos = "tea,tna,tem";//problemas de punto y la coma
				$tasas->valores = "'".round($data["consulta"]["tea"],3).
								  "','".round($linea[4],3).
								  "','".round($data["consulta"]["tem"],3)."'";
				//echo $tasas->valores;
				if (!$tasas->modificarSQL()) 
				{
					echo "Datos modificados correctamente <br/>";
				}
				else
				{
					echo "Datos no modificados correctamente <br/>";
				}			
			}
			echo $i."<br/>";
			++$i;
		}	
	
		echo view('footer');		
	}

	
}
