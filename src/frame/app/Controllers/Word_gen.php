<?php
/**
 * Trabajar con documentos de Word y PHP usando PHPOffice
 *
 * Más tutoriales en: parzibyte.me/blog
 *
 * Ejemplo 1:
 * Crear documento de word, poner propiedades,
 * guardar para versiones actuales y
 * establecer idioma
 */
namespace App\Controllers;
//require "vendor/autoload.php";
use PhpOffice\PhpWord\Style\Language;

class Word_gen extends BaseController 
{
    public function preview($ip="")
    {
        $tabla = new Sql_lib();
		$tabla->tabla = "general";
        $tabla->condicion = "ip='".$ip."'";
        $data["tabla"] = $tabla->consultaSQLbasicaRow(); 
		$detalle = new Sql_lib();
		$detalle->tabla = "detalle";
        $detalle->condicion = "ip='".$ip."'";
        $data["vector"] = $detalle->consulSQLbasica();

        $documento = new \PhpOffice\PhpWord\PhpWord();
        $sectionStyle = array('orientation' => null,
                            'marginLeft' => 900,
                            'marginRight' => 900,
                            'marginTop' => 900,
                            'marginBottom' => 900);
        $seccion = $documento->addSection($sectionStyle);
         //$seccion->addTextBreak();
        $seccion->addImage(site_url("media/img/logo.png"),
            ['width'=>500, 'height'=>150, 'align'=>'center'] );
        $text_style = ["bold"=>true,'spaceAfter'=>60, 'name'=>'Tahoma', 
                        'size'=>12];    
        $seccion->addText( 'Total Adeudado: ', $text_style);
        $seccion->addText( '           Fecha de Liquidacion: '.
                            $data["tabla"]["fecha_close"].
                          '            Total Adeudado: '.
                          $data["tabla"]["neto"], $text_style);
        $seccion->addTextBreak();
        $seccion->addText( 'Información Adicional',$text_style);
        $seccion->addText( '        Total Capital: '.
                            $data["tabla"]["efectivo"].
                            '       Total Intereses: '.
                            $data["tabla"]["total_int"].
                            '       Total Pagos: '.
                            $data["tabla"]["total_pago"],$text_style);
        $seccion->addTextBreak();

        $head_table =['bgColor'=>'00114F'];
        $tableStyle = ["borderColor" => "E56300","alignment" => "center",
                                                "borderSize" => 5];
        $table = $seccion->addTable( $tableStyle, $head_table);
        $table->addRow();
        $table->addCell()->addText("Concepto",$text_style);
        $table->addCell()->addText("Inicio",$text_style);
        $table->addCell()->addText("Final",$text_style);
        $table->addCell()->addText("Capital",$text_style);
        $table->addCell()->addText("Pagos",$text_style);
        $table->addCell()->addText("TEA",$text_style);
        $table->addCell()->addText("Tasa",$text_style);
        $table->addCell()->addText("Interes",$text_style);
        $table->addCell()->addText("Acumlulado",$text_style);
        $table->addCell()->addText("Saldo $",$text_style);         

        foreach($data["vector"] as $row)
        {
            $table->addRow();
            $table->addCell()->addText($row["concepto"]);
            $table->addCell()->addText($row["inicio"]);
            $table->addCell()->addText($row["fin"]);
            $table->addCell()->addText($row["capital"]);
            $table->addCell()->addText($row["pago"]);
            $table->addCell()->addText($row["tea"]);
            $table->addCell()->addText($row["tasa"]);
            $table->addCell()->addText($row["interes"]);
            $table->addCell()->addText($row["acumulado"]);
            $table->addCell()->addText($row["saldo_liquidar"]);
        }    

        $propiedades = $documento->getDocInfo();
        $propiedades->setCreator("Ariel Marcelo Diaz");
        $propiedades->setCompany("Infrasoft Servicios Informaticos");
        $propiedades->setTitle("Calculo de costos");
        $propiedades->setDescription("Informe de creacion de costos");
        $propiedades->setCategory("Tutoriales");
        $propiedades->setLastModifiedBy("Ariel Marcelo Diaz");
        $propiedades->setCreated(mktime());
        $propiedades->setModified(mktime());
        $propiedades->setSubject("Asunto");
        $propiedades->setKeywords("documento, php, word");
        # Para que no diga que se abre en modo de compatibilidad
        $documento->getCompatibility()->setOoxmlVersion(15);
        # Idioma español de México
        $documento->getSettings()->setThemeFontLang(new Language("ES-MX"));
        # Enviar encabezados para indicar que vamos a enviar un documento de Word
        $nombre = "calculo.docx";
        header("Content-Description: File Transfer");
        header('Content-Disposition: attachment; filename="' . $nombre . '"');
        header('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
        header('Content-Transfer-Encoding: binary');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Expires: 0');
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($documento, "Word2007");
        # Y lo enviamos a php://output
        $objWriter->save("php://output");
    }
}


?>