<?php 
/***************************************************
           https://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: https://infrasoft.com.ar
***************************************************/
//print_r($vector);
?>
<div class="container">
    <h1>Calculos Judiciales</h1>
	<div class="panel">
       <form method="post" accept-charset="utf-8"> 
    <div class="row">   
        <div class="col-auto mt-1 pt-1">  
					<div class="input-group">
             <span class="input-group-text">Concepto</span>
             <input type="text" class="form-control" id="concepto" 
                  name="concepto" placeholder="Ingrese el concepto" 
                  value="<?=$vector["concepto"];?>" required />                 
          </div>
        </div>
				
        <div class="col-auto mt-1 pt-1">
					<div class="input-group">
             <span class="input-group-text">Fecha de inicio</span>
             <input type="date" class="form-control" id="date_ini" 
                  name="date_ini" placeholder="Ingrese el concepto" 
                  value="<?=$vector["inicio"];?>" required />                 
					</div>
        </div>
				<div class="col-auto mt-1 pt-1">  
					<div class="input-group">		
             <span class="input-group-text">Capital</span>
             <input type="number" step="0.01" class="form-control" 
                  id="capital" name="capital"
                  value="<?=round($vector["capital"], 2);?>" 
                  placeholder="Ingrese el concepto" required />                 
					</div>
        </div>
				  
				<div class="col-auto m-1 p-1">                
          <div class="input-group has-validation"> 
            <span class="input-group-text">Tipo de Tasa</span>
            <select class="form-select" id="tasa_sel" name="tasa_sel" >                    
              <option value="1">Tasas Activa descuento de documentos en pesos H/59 días (BTF) </option>
              <option value="2">Tasas Activa descuento de documentos en pesos D/120 días (BTF)  </option>
              <option value="3">Tasas Pasiva Plazo fijo en pesos a 30 días (BTF)  </option>
              <option value="4">Promedio Activa -59/Pasiva (BTF)  </option>
              <option value="5">Promedio Activa +180/Pasiva (BTF)  </option>                    
            </select>                    
          </div>
        </div>

        <div class="col-auto mt-1 pt-1">  
				  <div class="input-group">		
             <span class="input-group-text">Pago</span>
                <input type="number" step="0.01" class="form-control" 
                    id="pago" name="pago" value="<?=round($vector["pago"], 2);?>"
                    placeholder="Ingrese el pago" required />                 
					</div>
        </div>

        <div class="text-center">
          <a href="<?php echo site_url("public");?>">
          <button type="button" class="btn btn-secondary" >
            <i class="far fa-window-close"></i> Regresar
          </button>
          </a>
          <button type="submit" class="btn btn-primary">
             <i class="far fa-edit"></i> Modificar  
          </button>
        </div>
        </div>
       </form>
       <?php echo $mensaje;?>
    </div>
</div>