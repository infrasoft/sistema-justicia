<?php 
/***************************************************
           https://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: https://infrasoft.com.ar
***************************************************/
if (!isset($otro_btn)) 
{
  $otro_btn = "";
  $aux="";
}
else
{
  $aux="hidden";
}
?>
<div class="container text-center">
      <?php echo "<a href='".site_url("public/index.php/home/op/".
                              $_SESSION["ip"]."/--/--/del_all"."/").
                              "' title='Eliminar Sesssion'>
                              <button type='button' class='btn btn-secondary m-1 p-1'>
                                <i class='fas fa-broom'></i> Limpiar Seccion
                              </button>
                              </a>";
        ?>                             
        <a href="<?=site_url("public/index.php/home/preview/".
                              $_SESSION["ip"]);  ?>" >
          <button type="button" class="btn btn-primary m-1 p-1" <?=$aux; ?>>
            <i class="fas fa-desktop"></i> Vista Previa
          </button>
        </a>
        <a href="<?=site_url("public/index.php/Pdf_print/print_form/-/".
                              $_SESSION["ip"]);?>/" >        
        <button type="button" class="btn btn-danger m-1 p-1">
             <i class="far fa-file-pdf"></i> PDF
        </button>
        </a>
        <a href="<?=site_url("public/index.php/Word_gen/preview/".
                            $_SESSION["ip"]); ?>/" target="_blank">
        <button  type="button" class="btn btn-primary m-1 p-1">
          <i class="far fa-file-word"></i> Word
        </button>
        </a>
        <a href="<?=site_url("public/index.php/Pdf_print/print_form/preview/".
                  $_SESSION["ip"]."/"); ?>" target="_blank" >
        <button type="button" class="btn btn-warning m-1 p-1">
            <i class="fas fa-print"></i> Imprimir
        </button>
        </a>
        <?=$otro_btn;?>
		</div>