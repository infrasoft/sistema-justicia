<?php 
/***************************************************
           https://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: https://infrasoft.com.ar
***************************************************/

?>
<div class="col-12 mt-1 pt-1">  
					<div class="input-group">
                      <span class="input-group-text">Concepto</span>
                      <input type="text" class="form-control" id="concepto" name="concepto" 
                            placeholder="Ingrese el concepto" required />                 
                    </div>
                  </div>
				  <div class="col-12 mt-1 pt-1">
						<div class="input-group">
                      <span class="input-group-text">Fecha de inicio</span>
                      <input type="date" class="form-control" id="date_ini" name="date_ini" 
                            placeholder="Ingrese el concepto" required />                 
						</div>
                  </div>
				  <div class="col-12 mt-1 pt-1">  
					<div class="input-group">		
                      <span class="input-group-text">Capital</span>
                      <input type="number" step="0.01" class="form-control" id="capital" name="capital" 
                            placeholder="Ingrese el capital" required />                 
					</div>
                  </div>
				  
				      <!-- <div class="col-12 mt-1 pt-1">                
                <div class="input-group has-validation">
                  <span class="input-group-text">Tipo de Tasa</span>
                  <select class="form-select" aria-label="Default select" id="tasa_sec" name="tasa_sec"
                      onchange="tasa_parametro()">                    
                    <option value="1">Tasas Activa descuento de documentos en pesos</option>
                    <option value="2">Tasas Pasiva Plazo fijo en pesos a 30 días (BTF) </option>
                    <option value="3">Promedio Activa </option>
                  </select>
                    
                </div>
              </div>-->

              <div class="col-12 mt-1 pt-1">  
					      <div class="input-group">		
                      <span class="input-group-text">Pago</span>
                      <input type="number" step="0.01" class="form-control" id="pago" name="pago" 
                            placeholder="Ingrese el pago" value="0" required />                 
					      </div>
              </div>