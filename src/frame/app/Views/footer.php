<?php 
/***************************************************
           https://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: https://infrasoft.com.ar
***************************************************/
?>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>

  <!-- Page level plugin JavaScript-->
<script src="https://code.jquery.com/jquery-3.5.1.js"></script> 	  
<script src="<?php echo site_url("media/datatables/jquery.dataTables.js");?>"></script> 
<script src="<?php echo site_url("media/datatables/dataTables.bootstrap4.js");?>"></script>  

<!-- Demo scripts for this page-->
<script src="<?php echo site_url("media/js/datatables-demo.js");?>"></script>

<script type="text/javascript">
$(document).ready(function() {
    $('#dataTable').dataTable( {
        "destroy":true,
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
        }
    } );
} );

</script>	
<script src="<?php echo site_url("media/js/form.js");?>"></script>
</body>    
</html>