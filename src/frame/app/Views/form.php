<?php 
/***************************************************
           https://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: https://infrasoft.com.ar
***************************************************/
//print_r($tabla);
use CodeIgniter\I18n\Time;
$hoy =getdate();
if ($hoy["mon"]<10) 
{
  $hoy["mon"] = "0".$hoy["mon"];
}
if ($hoy["mday"]<10) 
{
  $hoy["mday"] = "0".$hoy["mday"];
}
helper('vistas'); 

?>
<div class="container">
    <h1>Calculos Judiciales</h1>
	<div >
       <form method="post" accept-charset="utf-8">                 
          <div class="row">   
            <input type="date" id="date_creation" name="date_creation" 
                value="<?php echo $hoy["year"]."-".$hoy["mon"]."-".$hoy["mday"]; ?>" hidden/>                        
             <div class="col-auto m-1 p-1">                
                <div class="input-group has-validation">
                  <div class="text-danger">
                    <h3 style='font-weight:bold'>
                      1 - 
                    </h3>
                  </div>
                  <span class="input-group-text">Fecha de liquidacion</span>
                  <input type="date" class="form-control" id="date_end" name="date_end" 
                        placeholder="Ingrese la fecha de liquidacion" 
                        value="<?php echo $hoy["year"]."-".$hoy["mon"]."-".$hoy["mday"]; ?>"
                         required />                    
                </div>
              </div>

              <div class="col-auto m-1 p-1">                
                <div class="input-group ">
                  <div class="text-danger"> 
                    <h3  style='font-weight:bold' >
                      2 - 
                    </h3>
                  </div>
                  <span class="input-group-text">Tipo de Tasa</span>
                  <select class="form-select" id="tasa_sel" name="tasa_sel" >                    
                    <option value="1">Tasas Activa descuento de documentos en pesos H/59 días (BTF) </option>
                    <option value="2">Tasas Activa descuento de documentos en pesos D/180 días (BTF)  </option>
                    <option value="3">Tasas Pasiva Plazo fijo en pesos a 30 días (BTF)  </option>
                    <option value="4">Promedio Activa -59/Pasiva (BTF)  </option>
                    <option value="5">Promedio Activa +180/Pasiva (BTF)  </option>                                   
                  </select>                    
                </div>
              </div>
              
   		 </div>
       <div class="container">
				  <div class="text-center mt-3 pt-3"><div class="text-danger">
          <h3  style='font-weight:bold'>3 - </h3></div>
            <button type="button" class="btn btn-primary " data-bs-toggle="modal" 
                 data-bs-target="#exampleModal">
                 <i class="far fa-file"></i> Agregar Calculo
            </button>
				  </div>
        </div>

        <hr/>

		<?php
          echo view("botones");
    ?>

        <!-- Modal -->
 <div class="modal fade" id="exampleModal" tabindex="-1" 
                    aria-labelledby="ModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">PLANILLA DE CALCULOS JUDICIALES</h5>
                  <button type="button" class="btn-close" data-bs-dismiss="modal" 
                        aria-label="Close"></button>
            </div>
            <div class="modal-body">
              
                <?php
                      echo view('detalle');
                ?>
              
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">
                <i class="far fa-window-close"></i> Cancelar
              </button>
              <button type="submit" class="btn btn-primary">
                <i class="far fa-plus-square"></i> Adjuntar Calculo
              </button>
            </div>
          </div>
        </div>
      </div>
      </form>

      <!-- DataTables Example -->
      <div class="card mb-3 " <?php
                                    
                                    if ((!isset($vector)) OR $vector->num_rows =="0") 
                                    {
                                      echo "hidden";
                                    }
      ?>>
        <div class="card-header">
          <i class="fas fa-table"></i>
          Lista 
        </div>
        <div class="card-body">
          <div class="table-responsive">
      <!--table-->
      <table id="tableCal" name="tableCal" class="table table-striped table-hover" style="width:100%">
        <thead class="text-primary">
          <td>Concepto</td>          
          <td>Fecha Inicio</td>
          <td>Fecha Final</td>          
          <td>Capital</td>
          <td>Pagos</td>
          <td>TEA</td>
          <td>Tasa</td>
          <td>Interes</td>
          <td>Saldo Int</td>
          <td>Saldo $$$</td>
          <td>OP</td>
        </thead>
        <tfoot  class="text-primary">
          <td>Concepto</td>          
          <td>Fecha Inicio</td>
          <td>Fecha Final</td>          
          <td>Capital</td>
          <td>Pagos</td>
          <td>TEA</td>
          <td>Tasa</td>
          <td>Interes</td>
          <td>Saldo Int</td>
          <td>Saldo $$$</td>
          <td>OP</td>
        </tfoot>
		<tr>
			<?php 
            if (isset($vector)) 
            {   // print_r($tabla);          
              foreach ($vector as $row) 
              {                              
                echo "<tr>
                          <td>".$row["concepto"]."</td>
                          <td>".invierte_fecha($row["inicio"])."</td>
                          <td>".invierte_fecha($row["fin"])."</td>
                          <td>".$row["capital"]."</td>
                          <td>".$row["pago"]."</td>
                          <td>".$row["tea"]."</td>
                          <td>".$row["tasa"]."</td>                         
                          <td>$ ".$row["interes"]."</td>
                          <td>$ ".$row["acumulado"]."</td>
                          <td>$ ".$row["saldo_liquidar"]."</td>
                          <td>                               
                              <a href='".site_url("public/index.php/home/op/".
                              $_SESSION["ip"]."/".$row["session"]."/".$row["id"]."/"."update"."/").
                              "' title='Modificar Item'>
                              <button type='button' class='btn btn-primary m-1 p-1'>
                                  <i class='far fa-edit'></i> Editar
                              </button>
                              </a>
                              <a href='".site_url("public/index.php/home/op/".
                              $_SESSION["ip"]."/".$row["session"]."/".$row["id"]."/").
                              "' title='Eliminar Elemento'>
                              <button type='button' class='btn btn-danger m-1 p-1'>
                                  <i class='fas fa-trash-alt' ></i> Eliminar                                
                              </button>
                              </a> 
                          </td>
                      </tr>";                      
              }
            // print_r($vector); 
            }             
            ?>
		</tr>
      </table>
    </div>
  </div>
  <p class="text-primary text-center">Total a liquidar: <b>$<?=$tabla["neto"];?></b></p>  
</div>

 <p class="text-primary">    La presente fue confecionada mediante el sistema de liquidaciones
   en linea del Colegio Publicos de Abogados de Rio Grande, en conformidad a las pautas
   establecidas por los Articulos 770, 902 y 903 del Codigo Civil y Comercial de la Nacion.
          </p>
 <?php echo $mensaje; ?>
      </div>   
