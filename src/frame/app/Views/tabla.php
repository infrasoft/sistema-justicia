<?php 
/***************************************************
           https://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: https://infrasoft.com.ar
***************************************************/
//print_r($tabla);
?>

<div class="row panel" onload="javascript:window.print()"> 
  <hr/>

  <h2>Total Adeudado</h2>
    <div class="col-auto m-1 p-1">                
      <div class="input-group has-validation">
         <span class="input-group-text">Fecha de liquidacion</span>
          <input type="date" class="form-control" id="liquidacion" name="liquidacion" 
              placeholder="Ingrese la fecha de liquidacion" 
               value="<?php echo $tabla["fecha_close"]; ?>"
                 required />                    
      </div>
    </div>

    
    <div class="col-auto m-1 p-1">                
      <div class="input-group has-validation">
         <span class="input-group-text">Total Adeudado</span>
          <input type="text" class="form-control" id="total_adeudado" name="total_adeudado" 
              placeholder="Total adeudado" 
               value="<?php echo $tabla["neto"]; ?>"
                 required />                    
      </div>
    </div>
</div>

<div class="row panel">
  <hr/>
  <h2> Informacion Adicional</h2>
  <div class="col-auto m-1 p-1">                
      <div class="input-group has-validation">
         <span class="input-group-text">Total Capital</span>
          <input type="text" class="form-control" id="total_capital" name="total_capital" 
              placeholder="Total capital" 
               value="<?php echo $tabla["efectivo"];  ?>"
                 required />                    
      </div>
    </div>

    <div class="col-auto m-1 p-1">                
      <div class="input-group has-validation">
         <span class="input-group-text">Total Intereses</span>
          <input type="text" class="form-control" id="total_interes" name="total_interes" 
              placeholder="Total Intereses" 
               value="<?php echo $tabla["total_int"]; ?>"
                 required />                    
      </div>
    </div>
  <!--
    <div class="col-auto m-1 p-1">                
      <div class="input-group has-validation">
         <span class="input-group-text">Base Regulatoria</span>
          <input type="text" class="form-control" id="base_regulatoria" name="base_regulatoria" 
              placeholder="Base Regulatoria" 
               value="<?php  ?>"
                 required />                    
      </div>
    </div> -->

    <div class="col-auto m-1 p-1">                
      <div class="input-group has-validation">
         <span class="input-group-text">Total Pagos</span>
          <input type="text" class="form-control" id="total_pagos" name="total_pagos" 
              placeholder="Total Pagos" 
               value="<?php echo $tabla["total_pago"]; ?>"
                 required />                    
      </div>
    </div>
    <!--
    <div class="col-auto m-1 p-1">                
      <div class="input-group has-validation">
         <span class="input-group-text">Tasa Acumulada</span>
          <input type="text" class="form-control" id="tasa_acumulada" name="tasa_acumulada" 
              placeholder="Tasa Acumulada" 
               value="<?php  ?>"
                 required />                    
      </div>
    </div>-->
    <hr/>
</div>

<!-- DataTables Example -->
<div class="card mb-3 " >
        <div class="card-header">
          <i class="fas fa-table"></i>
          Lista 
        </div>
        <div class="card-body">
          <div class="table-responsive">
      <!--table-->
      <table id="vistaPrevia" name="vistaPrevia" class="table table-striped table-hover" style="width:100%">
        <thead class="text-primary">
          <td>Concepto</td>          
          <td>Fecha Inicio</td>
          <td>Fecha Final</td>          
          <td>Capital</td>
          <td>Pagos</td>
          <td>TEA</td>
          <td>Tasa</td>
          <td>Interes</td>
          <td>Acumulado</td>
        </thead>
        <tfoot class="text-primary">
          <td>Concepto</td>          
          <td>Fecha Inicio</td>
          <td>Fecha Final</td>          
          <td>Capital</td>
          <td>Pagos</td>
          <td>TEA</td>
          <td>Tasa</td>
          <td>Interes</td>
          <td>Acumulado</td>
        </tfoot>
		<tr>
			<?php 
            if (isset($detalle)) 
            {
              foreach ($detalle as $row) 
              {
                echo "<tr>
                          <td>".$row["concepto"]."</td>
                          <td>".invierte_fecha($row["inicio"])."</td>
                          <td>".invierte_fecha($row["fin"])."</td>                          
                          <td>".$row["capital"]."</td>
                          <td>".$row["pago"]."</td>
                          <td>".$row["tea"]."</td>
                          <td>".$row["tasa"]."</td>
                          <td>$ ".$row["interes"]."</td>
                          <td>$ ".$row["acumulado"]."</td>
                      </tr>";
              }
              // print_r($vector);
            }
             
            ?>
		</tr>
      </table>
    </div>
  </div>
  <p class="text-primary">Total a liquidar: $<?php echo $tabla["neto"]; ?> </p>
  
</div>