<?php 
/***************************************************
           https://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: https://infrasoft.com.ar
***************************************************/

$_SESSION["ip"] = $_SERVER['REMOTE_ADDR'];
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<!-- Define Charset -->
	<meta charset="utf-8">
    <link href="media/css/styles.css" rel="stylesheet" >
    <!-- Page level plugin CSS-->
    <link href="media/css/styles.css" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
   <title>Formulario de consulta</title>
   <link rel='shortcut icon' type='image/x-icon' href='https://www.infrasoft.com.ar/media/img/icono.ico' /> 
      

  <!-- Page level plugin CSS-->
  <link href="<?php echo site_url("media/datatables/dataTables.bootstrap4.min.css");?>" rel="stylesheet">

  <!-- Custom fonts for this template-->
  <link href="<?php echo site_url("media/fontawesome/css/all.min.css");?>" rel="stylesheet" type="text/css">
  <script src="<?php echo site_url("media/fontawesome/js/all.js");?>"></script>
  
  <link href="<?php echo site_url("media/css/styles.css");?>" rel="stylesheet">

</head>
<body>