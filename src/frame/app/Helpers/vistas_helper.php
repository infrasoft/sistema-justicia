<?php 
/***************************************************
           https://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: https://infrasoft.com.ar
***************************************************/
//if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//permite mostrar mensajes en la pantalla

	function mensajes($text='',$head='¡Informacion!',$alert = "alert-info")
	{
		return '<div class="alert '.$alert.' alert-dismissable">  					
					  <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
  
  					<strong>'.$head.'</strong> '.$text.'.
				</div>';
	}	
	//realiza la inversion de fechas para que este bien para el usuario
	function invierte_fecha($date='')
	{
		$fecha = substr($date, 8,2)."-".substr($date, 5,2)."-".substr($date, 0,4);
		return $fecha;
	}

?>
